# III. Overview Django Request flows

1. Overview Request flows :

![alt text](./image/home_page_1.png)


* Map Request URL từ user tới page xử lý được định nghĩa tại /local_lib/urls.py
* Chuyển tiếp request từ urls.py tới views.py tương ứng
* Views.py đọc hoặc ghi data từ Models.py, load template page tại Template.py và response.

2. Định nghĩa URL

* localhost/app_01 : Request index page
* localhost/books : Danh sách book
* localhost/authors : Danh sách tác giả
* localhost/books/<id> : Chi tiết 1 cuốn sách
* localhost/author/<id> : Thông tin chi tiết 1 tác giả 

# IV. Develop web
Tạo webpage sử dụng cho enduser

#### 1. Edit urls 
/app_01/urls.py


* Request : localhost/app_01
* Response : locahost/app_01/index.html


````
from django.urls import path
from app_01 import views

urlpatterns = [
    path('', views.index, name='index'),
]
````

#### 2. Views
/app_01/views.py

* Nhận request
* Select data từ Model, đếm hoặc filter số lượng từ models
* Response kết quả

````
from django.shortcuts import render
from app_01.models import Author, Genre, Book, BookInstance, Language


def index(request):

    # Dem so luong sach trong moi objects

    num_books = Book.objects.all().count()
    num_bookinstance = Book.objects.all().count()
    num_author = Author.objects.count()

    # Dem so luong sach con lai, co status = a
    num_instance_available = BookInstance.objects.filter(status__exact='a').count()


    context = {
        'num_books': num_books,
        'num_instance': num_bookinstance,
        'num_author': num_author,
        'num_instance_available': num_instance_available,
    }

    return render(request, 'index.html', context=context)
````

Note : Một số lệnh tương đương với sql như sau
- Book.objects = Select * from Book
- Book.objects.all().count() = Book.objects.count() = Select count(*) from Book
- Book.objects.filter(<Điều kiện>)
    - Điều kiện :
        - columnA__exact='values' : Select * from Book where columnA == 'values'
        - columnB__contains='values' : Select * from Book where columnB Like 'values'
        - More details : https://docs.djangoproject.com/en/2.1/ref/models/querysets/#field-lookups
        
   
- Return : index.html và context. Giá trị lưu tại context sẽ được sử dụng tại index.html


#### 3. Create HTML Templates và Index.html

##### 3.1 Config file settings.py


Web chứa một số nội dung static như javascript hoặc css, để khai báo nơi liên kết tới các file này, thực hiện các step sau.


**Step 1 :** Tạo file css tại folder /app_01/static/css/style.css
````
.sidebar-nav {
    margin-top: 20px;
    padding: 0;
    list-style: none;
}
````

**Step 2 :** Khai báo biến toàn cục STATIC_URL lưu PATH các biến static như javascript hoặc css

Edit /local_lib/settings.py

````
...
STATIC_URL = '/static/'
...
```` 

**Step 3 :** Edit Config

Khi views gọi tới index.html, django sẽ tự động tìm kiếm trong TEMPLATES.
* os.path.join(BASE_DIR, 'templates')

````
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
````

##### 3.2 base_generic.html

/templates/base_generic.html : File HTML này sử dụng lưu một số thành phần dùng chung trong HTML như Header hoạc Footer

````
<!DOCTYPE html>
<html lang="en">
<head>
  {% block title %}<title>Local Library</title>{% endblock %}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  
  <!-- Add additional CSS in static file -->
  {% load static %}
  <link rel="stylesheet" href="{% static 'css/styles.css' %}">
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-2">
      {% block sidebar %}
        <ul class="sidebar-nav">
          <li><a href="{% url 'index' %}">Home</a></li>
          <li><a href="">All books</a></li>
          <li><a href="">All authors</a></li>
        </ul>
     {% endblock %}
      </div>
      <div class="col-sm-10 ">{% block content %}{% endblock %}</div>
    </div>
  </div>
</body>
</html>
````


Note : Template dùng chung khai báo một số block, có thể overwrite lại tại các view kế thừa

* Cấu trúc block. base_generic.html
    ````
    {% block <Tên Block> %}
        <html>
    {% endblock %}
    ````

* index.html Kế thừa từ base_generic:
    ````
    {% extends "base_generic.html" %}
    ```` 

* Khai báo file static css sử dụng.
    * Step 1 : Load biến static toàn cục, định nghĩa nơi chứa các biến static tĩnh
        ````
        {% load static %}
        ````
    * Step 2 : Chỉ định cụ thể file sử dụng
        ````
        <link rel="stylesheet" href="{% static 'css/styles.css' %}">
        ````
        
    * Step 3 : Tương tự với các biến khác, ví dụ image
        ````
        <img src="{% static 'app_01/images/local_library_model_uml.png' %}" alt="UML diagram" style="width:555px;height:540px;">
        ````


##### 3.3 Index.html

* Kế thừa lớp dùng chung, khai báo tại base_generic.html
* Show giá trị được lưu tại context được truyền từ views.

```` 
{% extends "base_generic.html" %}

{% block content %}
  <h1>Local Library Home</h1>
  <p>Welcome to LocalLibrary, a website developed by <em>Mozilla Developer Network</em>!</p>
  <h2>Dynamic content</h2>
  <p>The library has the following record counts:</p>
  <ul>
    <li><strong>Books:</strong> {{ num_books }}</li>
    <li><strong>Number Instances:</strong> {{ num_instance }}</li>
    <li><strong>Number author:</strong> {{ num_author }}</li>
    <li><strong>Number Available Instances:</strong> {{ num_instance_available }}</li>
  </ul>
{% endblock %}
```` 





