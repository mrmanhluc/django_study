# Basic.
## I. Django Setup

....
Updating
....

## II. Django-admin và manage.py

Manage.py là file được tạo tự động khi 1 project Django được tạo.

manage.py có chức năng giống django-admin nhưng đảm nhiệm một số phần tự động như : 

- Đặt một số package của project vào sys.path

- Đặt một số thuộc tính khai bào tại settings.py vào DJANGO_SETTINGS_MODULE

Cú pháp run django-admin hoặc manage.py
````
django-admin <command> [Options]
or
manage.py <command> [Options]
or
python -m django command [Options]
````

#### 1. Manage.py

Start server
````
python manage.py runserver 192.168.11.21:80
````

Reload Database sau khi thay đổi Models
```bash
python manage.py makemigrations 
python manage.py migrate
```

#### 2. Django Admin
Là tools giúp quản lý project dễ hơn

##### 2.1 Sử dụng django-admin, tạo project django và các lib cần sử dụng
````commandline
mkdir project-name
cd project-name
django-admin startproject locallibrary
````

Kết quả : 
```commandline
locallibrary/
    manage.py
    locallibrary/
        __init__.py
        settings.py
        urls.py
        wsgi.py
```

Ý nghĩa các file : 

- __init__.py : Empty file để xác định structs python như một Python Package
- setting.py : Toàn bộ setting web sẽ được đặt tại đây gồm register 1 app, location của file, database config details
- urls.py : Định nghĩa url view mapping. Toàn bộ url liên kết trong web sẽ định nghĩa tại đây
- wsgi.py : Sử dụng giúp Django application giao tiếp với web server.
- manage.py : Script dùng để tạo ra application, làm việc với Database và start một developent web server


##### 2.2 Sử dụng manage.py tạo Application

```commandline
python3.6 manage.py startapp app_01
```

Kết quả :

```commandline
locallibrary/
    manage.py
    locallibrary/
    app01/
        admin.py
        apps.py
        models.py
        tests.py
        views.py
        __init__.py
        migrations/
In
```

Trong đó : 

- migrations folder : Folder chứa file có thể tự động upadte vào database cũng như modify lại models
- views.py : Chứa layer view
- models.py : Chứa layer model
- tests.py : Layer test
- views.py : Layer view


#### 3 Setting project Django
 
##### 3.1. Registering app_01 Application

Mỗi application cần register với projects.

edit : ./locallibrary/settings.py, thêm dòng **'app_01.apps.App01Config'**

```commandline
    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'app_01.apps.App01Config'
    ]
```

##### 3.2 Register Database

Edit file ./locallibrary/settings.py

- Đối với trường hợp sử dụng sqlite

```commandline
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```

- Đối với trường hợp sử dụng Mysql

```commandline
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'DB_NAME',
        'USER': 'DB_USER',
        'PASSWORD': 'DB_PASSWORD',
        'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
    }
}
```

##### 3.3 URL Register

URL mapper file là file định nghĩa liên kết URL trong app.

./locallibbrary/urls.py

Step 1 : Định nghĩa path trong app_01

* Case 1 : Chỉ muốn add một số urls trong app01 vào url patterns
```commandline
from django.contrib import admin
from django.urls import path

urlpatterns += [
    path('app01/', include('app01.urls')),
]
```

* Case 2 : Muốn chuyển hướng Request từ localhost -> localhost\app01\
```commandline
from django.contrib import admin
from django.urls import path
from django.views.generic import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
]

# Add path of app_01
urlpatterns += [
    path('', RedirectView.as_view(url='/app_01/', permanent=True)),
]
```

Step 2 : Định nghĩa URL trong app01

Tạo file .\app_01\
```commandline
mkdir .
```

#### 4. Administrator

Page sử dụng quản lý user

Step 1 : Create account to login admin page
```commandline
python manage.py createsuperuser
```

Step 2 : Start server
```commandline
python manage.py runserver
```

Step 3 : Request URL
```commandline
http:\\localhost\admin
``` 

# III. Django Model
Django có 4 layer chính gồm : 
- Models
- View
- Template
- Forms


#### 1. Database Structure

Cho cấu trúc quản lý sách, gồm các objects sau : 

- BookInstance - Định danh từng cuốn sách
    - uniqueID (String)
    - due_back (DataField)
    - status (boolean load status)
    - book -> FK.Book (n-1)

- Book - Chi tiết sách
    - title (String)
    - author -> FK.Author (1-n)
    - summary (String)
    - imprint (String)
    - ISBN (String)
    - genre -> FK.Genre
    - language -> FK.Language
    
- Author 
    - name (String)
    - date_of_birth : DateField
    - date_of_death : DateField
    - books : FK (1-n)

- Genre : Thể loại
    - id PK (String)
    - name (String)

- Language : 
    - id PK (String)
    - name (String)
    
#### 2. Tạo Models

./app_01/models.py

```python
from django.db import models
import uuid


# Create your models here.
class Book(models.Model):

    # Create fields
    title = models.CharField(max_length=100, help_text='Title of book')

    author = models.ForeignKey(
        'Author',
        help_text='Author name',
        on_delete=models.SET('Unknown'),
        null=True
    )
    summary = models.TextField(max_length=500, help_text='Summary of book')
    imprint = models.CharField(
        max_length=200,
        blank=True,# Cho phep nhan gia tri blank
        null=True,# Neu null, Django insert gia tri null vao
    )
    isbn = models.CharField(max_length=200, null=True)

    genre = models.ForeignKey(
        'Genre',
        on_delete=models.SET_NULL,
        null=True,
    )

    language = models.ForeignKey(
        'Language',
        on_delete=models.SET_NULL,
        null=True,

    )

    class Meta:
        # Data tra ve luon duoc return gia tri giam dan theo author name
        ordering = ['-author']

    # Return string thể hiện ý nghĩa của objects. Được hiển thị trên màn hình administrator
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reversed('book-detail', args=[str(self.id)])


class BookInstance(models.Model):
    """
        Bookinstance : Quản lý tình trạng mượn của từng cuốn sách
    """

    # UniqueID được tăng tự động
    id = models.UUIDField(
        # Define gia tri primary
        primary_key=True,
        default=uuid.uuid4
    )

    dua_back = models.DateField(
        # Tự động fillin khi model được save
        auto_now=True,
    )

    LOAN_STATUS = (
        ('m', 'Maintenance'),
        ('o', 'On loan'),
        ('a', 'Available'),
        ('r', 'Reserved'),
    )

    status = models.CharField(
        default='m',
        blank=False,
        choices=LOAN_STATUS,
        help_text='Book availability',
        max_length=100
    )

    book = models.ForeignKey(
        'Book',
        on_delete=models.SET_NULL,
        null=True,
    )

    class Meta:
        ordering = ['-dua_back']

    def __str__(self):
        return f'{self.id} ({self.book.title})'


class Author(models.Model):

    name = models.TextField(
        max_length=100,
    )

    date_of_birth = models.DateField(
        null=True,
        blank=True,
    )

    date_of_death = models.DateField(
        # Change name
        verbose_name='Died',
        null=True,
        blank=True,
    )

    class Meta:
        ordering = ['date_of_birth']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reversed('author-detail', args=[str(self.id)])


class Genre(models.Model):

    id = models.CharField(
        primary_key=True,
        max_length=30
    )
    name = models.CharField(
        max_length=100,
    )

    def __str__(self):
        return self.name


class Language(models.Model):

    id = models.CharField(
        primary_key=True,
        max_length=30
    )

    name = models.CharField(
        max_length=100,
        null=False,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reversed('detail-language', args=[str(self.id)])


    class Meta:
        ordering = ['id']

```

Run câu lệnh sau để thực hiện create hoặc update lại database
```bash
python manage.py makemigrations 
python3 manage.py migrate
```

Kết quả : 
```commandline
Migrations for 'app_01':
  app_01\migrations\0001_initial.py
    - Create model Author
    - Create model Book
    - Create model BookInstance
    - Create model Genre
    - Create model Language
    - Add field genre to book
    - Add field language to book
    
....
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying app_01.0001_initial... OK

```


##### Note : 

- Cấu trúc tổng quát Models trong Django.

```python
from django.db import models

# MyModelName, ví dụ book.
class MyModelName(models.Model):
    """A typical class defining a model, derived from the Model class."""

    # Fields, định nghĩa các filed
    my_field_name = models.CharField(max_length=20, help_text='Enter field documentation')
    ...

    # Metadata
    class Meta: 
        ordering = ['-my_field_name']

    # Methods
    def get_absolute_url(self):
        """Returns the url to access a particular instance of MyModelName."""
        return reverse('model-detail-view', args=[str(self.id)])
    
    def __str__(self):
        """String for representing the MyModelName object (in Admin site etc.)."""
        return self.my_field_name

```

* Một số field quan trọng.

* Field
    - Field_type. Một số loại field thường dùng
        - CharField : Kiểu char có length ngắn, bắt buộc phải định nghĩa max_length
        - TextField : Kiểu text có length dài
        - IntegerField : Integer
        - DataField và DataTimeField : Lưu giá trị tương ứng với datetime.date và datetime.datetime objects. Thuộc tính quan trọng auto_now = True, trả về ngày giờ hiện tại mỗi khi model được save, auto_now_add trả về giờ hiện tại.
        - EmailField : Định dạng email
        - FileField và ImageField : Sử dụng Upload file hoặc Image, có một vài paramecter định nghĩa nơi lưu trữ file được lưu
        - AutoField : Tăng tự động, data type integer
        - ForeignKey : Sử dụng trong join 1 - n.
        - ManyToMany : Sử dụng trong join n - n. Có một vài paramecter định nghĩa acction khi delete 1 key có relationship. on_delete=models.SET_NULL, định nghĩa set null cho relation khi bị delete
        
    - Argument. 
        - help_text : Text label sử dụng trong HTML
        - verbose_name : Text giải thích
        - default : giá trị default cho field
        - null : True, Django lưu giá trị null xuống DB, default = False
        - black : True, Chấp nhận cho columns này Null
        - choices : Chỉ chấp nhận giá trị trong list cho trước
        - primary_key : Set primary key
        
* Model Metadata - Một số common attribute gồm ordering, verbose_name
    - Khi thực hiện 1 câu query gọi tới model, data sẽ sắp xếp theo thứ tự được định nghĩa ở bên dưới
    - ordering = ['-col1', 'col2'] : Return data sẽ giảm dần tại col1 và tăng dần tại col2
    - verbose_name = 'Tên đỡ rườm rà hơn' : Đổi tên cho class trong tình huống tên class quá rườm rà
    - Một số attribute khác cho phép apply access permission cho từng Object tương ứng.
    
* Method Model
    - __str__() : Mọi model đều nên định nghĩa method này để return human-readable cho mỗi class. Đoạn String này sẽ được hiển thị trên màn hình administrator
    - get_absolute_url(self) : Sử dụng trong tình huống muốn xem chi tiết từng data, Django sẽ tự động tạo button **View On Site**
    
    
#### 3. Admin

##### 3.1 Register Models into Admin Page
Để quản lý những model được tạo ra Step 2, cần khai báo với Admin

**Step 1.** Add model vào admin page.

./app_01/admin.py, add toàn bộ 5 models vào admin.py
```python
from django.contrib import admin

# Register your models here.

from app_01.models import Author, Genre, Book, BookInstance, Language

admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(BookInstance)
admin.site.register(Language)
```

**Step 2.** Tạo account login admin page.
```python
python manage.py createsuperuser
```

**Step 3.** Start Server và login 
```commandline
python manage.py runserver
```

Kết quả :

- Login Page : 

![alt text](./image/admin_login_1.png)

- Admin Page

![alt text](./image/admin_login_2.png)

- Add một ebook

![alt text](./image/admin_login_3.png)

- Danh sách ebook

![alt text](./image/admin_login_4.png)


##### 3.2 Modify admin Page.

Trong tình huống muốn chỉnh sửa nội dung hiển thị trên màn hình Admin, cần chỉnh lại tại **./app_01/admin.py**

Old : 
```python
from django.contrib import admin

# Register your models here.

from app_01.models import Author, Genre, Book, BookInstance, Language

admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(BookInstance)
admin.site.register(Language)
```

New : 
```python
from django.contrib import admin

# Register your models here.

from app_01.models import Author, Genre, Book, BookInstance, Language

admin.site.register(Book)
# admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(BookInstance)
admin.site.register(Language)


# Change Author showing in admin page
# Modified columns hiển thị trên Author
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('name', 'date_of_birth')

admin.site.register(Author, AuthorAdmin)
```

Note : list_display cho phép hiển thị số lượng columns nhiều hơn thay vì chỉ mình name.

Kết quả : 

![alt text](./image/admin_login_5.png)

Tham khảo một số tùy chọn nâng cao tại link : 
https://docs.djangoproject.com/en/dev/ref/contrib/admin/#modeladmin-objects






-----------


Link tham khảo : 
- https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Models