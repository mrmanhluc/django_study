from django.urls import path
from app_01 import views

urlpatterns = [
    path('', views.index, name='index'),
]
