from django.shortcuts import render
from app_01.models import Author, Genre, Book, BookInstance, Language


def index(request):

    # Dem so luong sach trong moi objects

    num_books = Book.objects.all().count()
    num_bookinstance = Book.objects.all().count()
    num_author = Author.objects.count()

    # Dem so luong sach con lai, co status = a
    num_instance_available = BookInstance.objects.filter(status__exact='a').count()


    context = {
        'num_books': num_books,
        'num_instance': num_bookinstance,
        'num_author': num_author,
        'num_instance_available': num_instance_available,
    }

    return render(request, 'index.html', context=context)

