from django.contrib import admin

# Register your models here.

from app_01.models import Author, Genre, Book, BookInstance, Language

admin.site.register(Book)
# admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(BookInstance)
admin.site.register(Language)


# Change Author showing in admin page
# Modified columns hiển thị trên Author
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('name', 'date_of_birth')

admin.site.register(Author, AuthorAdmin)
