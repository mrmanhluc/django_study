from django.db import models
import uuid


# Create your models here.
class Book(models.Model):

    # Create fields
    title = models.CharField(max_length=100, help_text='Title of book')

    author = models.ForeignKey(
        'Author',
        help_text='Author name',
        on_delete=models.SET('Unknown'),
        null=True
    )
    summary = models.TextField(max_length=500, help_text='Summary of book')
    imprint = models.CharField(
        max_length=200,
        blank=True,# Cho phep nhan gia tri blank
        null=True,# Neu null, Django insert gia tri null vao
    )
    isbn = models.CharField(max_length=200, null=True)

    genre = models.ForeignKey(
        'Genre',
        on_delete=models.SET_NULL,
        null=True,
    )

    language = models.ForeignKey(
        'Language',
        on_delete=models.SET_NULL,
        null=True,
    )

    class Meta:
        # Data tra ve luon duoc return gia tri giam dan theo author name
        ordering = ['-author']

    # Return string thể hiện ý nghĩa của objects. Được hiển thị trên màn hình administrator
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reversed('book-detail', args=[str(self.id)])


class BookInstance(models.Model):
    """
        Bookinstance : Quản lý tình trạng mượn của từng cuốn sách
    """

    # UniqueID được tăng tự động
    id = models.UUIDField(
        # Define gia tri primary
        primary_key=True,
        default=uuid.uuid4
    )

    dua_back = models.DateField(
        # Tự động fillin khi model được save
        auto_now=True,
    )

    LOAN_STATUS = (
        ('m', 'Maintenance'),
        ('o', 'On loan'),
        ('a', 'Available'),
        ('r', 'Reserved'),
    )

    status = models.CharField(
        default='m',
        blank=False,
        choices=LOAN_STATUS,
        help_text='Book availability',
        max_length=100
    )

    book = models.ForeignKey(
        'Book',
        on_delete=models.SET_NULL,
        null=True,
    )

    class Meta:
        ordering = ['-dua_back']

    def __str__(self):
        return f'{self.id} ({self.book.title})'


class Author(models.Model):

    name = models.TextField(
        max_length=100,
    )

    date_of_birth = models.DateField(
        null=True,
        blank=True,
    )

    date_of_death = models.DateField(
        # Change name
        verbose_name='Died',
        null=True,
        blank=True,
    )

    class Meta:
        ordering = ['date_of_birth']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reversed('author-detail', args=[str(self.id)])


class Genre(models.Model):

    id = models.CharField(
        primary_key=True,
        max_length=30
    )
    name = models.CharField(
        max_length=100,
    )

    def __str__(self):
        return self.name


class Language(models.Model):

    id = models.CharField(
        primary_key=True,
        max_length=30
    )

    name = models.CharField(
        max_length=100,
        null=False,
    )

    def __str__(self):
        return self.name

